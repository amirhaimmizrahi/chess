const express = require("express");
const app = express();
const cors = require("cors");
var bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.json());

const GRID_SIZE = 8;
const grid = createGrid();
let currentPlayer = "white";


function createGrid() {
  const tempGrid = [];

  for (let x = 0; x < GRID_SIZE; x++) {
    tempGrid.push([]);

    for (let y = 0; y < GRID_SIZE; y++) {
      tempGrid[x].push(createNode([x, y]));
    }
  }

  initBlackPieces(tempGrid);
  initWhitePieces(tempGrid);

  return tempGrid;
}

function createNode(position) {
  return { position, type: "" };
}

function initBlackPieces(tempGrid) {
  tempGrid[0][0].type = "r";
  tempGrid[1][0].type = "kn";
  tempGrid[2][0].type = "b";
  tempGrid[3][0].type = "q";
  tempGrid[4][0].type = "k";
  tempGrid[5][0].type = "b";
  tempGrid[6][0].type = "kn";
  tempGrid[7][0].type = "r";

  for (let i = 0; i < 8; i++) {
    tempGrid[i][1].type = "p";
  }
}

function initWhitePieces(tempGrid) {
  tempGrid[0][7].type = "R";
  tempGrid[1][7].type = "KN";
  tempGrid[2][7].type = "B";
  tempGrid[3][7].type = "Q";
  tempGrid[4][7].type = "K";
  tempGrid[5][7].type = "B";
  tempGrid[6][7].type = "KN";
  tempGrid[7][7].type = "R";

  for (let i = 0; i < 8; i++) {
    tempGrid[i][6].type = "P";
  }
}

app.get("/get_grid", (req, res) => {
  res.json(JSON.stringify(grid));
});

app.post("/move_request", (req, res) => {
  console.log(req.body);
  const { start_pos, end_pos } = req.body;
  console.log(start_pos);
  console.log(end_pos);

  const valid_move = checkValidMove(start_pos, end_pos);

  console.log("server response: " + JSON.stringify({ status: valid_move }));
  res.json(JSON.stringify({ status: valid_move }));
});

const ROOK_ALLOWED_MOVES = {
  distance: -1,
  directions: [
    [1, 0],
    [-1, 0],
    [0, 1],
    [0, -1],
  ],
};

const BISHOP_ALLOWED_MOVES = {
  distance: -1,
  directions: [
    [1, 1],
    [-1, 1],
    [1, -1],
    [-1, -1],
  ],
};

const QUEEN_ALLOWED_MOVES = {
  distance: -1,
  directions: [
    ...ROOK_ALLOWED_MOVES.directions,
    ...BISHOP_ALLOWED_MOVES.directions,
  ],
};

const KING_ALLOWED_MOVES = {
  distance: Math.sqrt(2),
  directions: [...QUEEN_ALLOWED_MOVES.directions],
};

const KNIGHT_ALLOWED_MOVES = {
  distance: Math.sqrt(5),
  directions: [
    [0.5, 1],
    [-0.5, 1],
    [0.5, -1],
    [-0.5, -1],
    [1, 0.5],
    [1, -0.5],
    [-1, 0.5],
    [-1, -0.5],
  ],
};

const WHITE_PAWN_ALLOWED_MOVES = {
  distance: 1,
  directions: [[0, -1]],
};

const BLACK_PAWN_ALLOWED_MOVES = {
  distance: 1,
  directions: [[0, 1]],
};

const PIECE_ALLOWED_MOVES = {
  r: ROOK_ALLOWED_MOVES,
  kn: KNIGHT_ALLOWED_MOVES,
  b: BISHOP_ALLOWED_MOVES,
  q: QUEEN_ALLOWED_MOVES,
  k: KING_ALLOWED_MOVES,
  p: BLACK_PAWN_ALLOWED_MOVES,
  P: WHITE_PAWN_ALLOWED_MOVES,
};

function checkValidMove(start, end) {
  let start_piece_type = grid[start[0]][start[1]].type;
  let end_piece_type = grid[end[0]][end[1]].type;

  if (end_piece_type.toLowerCase() == "k") return false; // can't eat a king
  if (start_piece_type == "") return false; // can't move empty piece

  if (!isDifferentColor(start_piece_type, end_piece_type)) return false; // can't eat my own piece

  if(!isPlayerTurn(start_piece_type)) return false; // it's not the player's turn

  if (start_piece_type !== "P") // black and white pawns move in different ways
    start_piece_type = start_piece_type.toLowerCase();

  if(!isMoveAllowed(start, end, PIECE_ALLOWED_MOVES[start_piece_type])) return false; // check if the piece can potentially get to end position

  if(!isNoObstacles(start, end)) return false; // check if there are no obstacles in the piece's way (from start to end)

  updateGrid(start, end); // update board
  currentPlayer = (currentPlayer == "white") ? "black" : "white";
  return true;
}

function isPlayerTurn(start_piece_type)
{
  const start_piece_case =
    start_piece_type == start_piece_type.toLowerCase() ? "l" : "u";

  return (((start_piece_case == "u") && currentPlayer == "white") || ((start_piece_case == "l") && currentPlayer == "black")) 
}

function isDifferentColor(start_piece_type, end_piece_type) {
  const start_piece_case =
    start_piece_type == start_piece_type.toLowerCase() ? "l" : "u";
  const end_piece_case =
    end_piece_type == end_piece_type.toLowerCase() ? "l" : "u";

  if (end_piece_type != "") {
    if (start_piece_case == end_piece_case) {
      return false;
    }
  }

  return true;
}

function updateGrid(start, end) {
  grid[end[0]][end[1]].type = grid[start[0]][start[1]].type;
  grid[start[0]][start[1]].type = "";
}

app.listen(5000, () => {
  console.log("Server started on port 5000");
});

function isNoObstacles(start, end) {
  if (grid[start[0]][start[1]].type.toLowerCase() == "kn") return true; // knight doesn't have obstacles in its way

  const dx = end[0] - start[0];
  const dy = end[1] - start[1];

  const moveDirection = [
    dx != 0 ? dx / Math.abs(dx) : 0,
    dy != 0 ? dy / Math.abs(dy) : 0,
  ]; // diving by the absolute value to get -1 or 1 (the direction)

  let current_pos = start;

  while (current_pos[0] !== end[0] || current_pos[1] !== end[1]) {
    current_pos = [
      current_pos[0] + moveDirection[0],
      current_pos[1] + moveDirection[1],
    ];

    if (current_pos[0] !== end[0] || current_pos[1] !== end[1]) {
      if (!isCellEmpty(current_pos)) {
        return false;
      }
    }
  }

  return true;
}

function isCellEmpty(position) {
  return grid[position[0]][position[1]].type == "";
}
function isMoveAllowed(start, end, allowedMoves) {
  const dx = end[0] - start[0];
  const dy = end[1] - start[1];

  const max = Math.max(Math.abs(dx), Math.abs(dy));
  const moveDirection = [dx / max, dy / max];
  const moveDistance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

  if (allowedMoves.distance < moveDistance && allowedMoves.distance !== -1)
    return false;

  for (const direction of allowedMoves.directions) {
    if (direction[0] === moveDirection[0] && direction[1] === moveDirection[1])
      return true;
  }

  return false;
}
