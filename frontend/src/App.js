import { Box } from "@mui/material";
import Grid from "./components/grid/Grid";
import GridProvider from "./contexts/GridProvider";

function App() {
  return (
    <GridProvider>
      <Box sx={{ width: "100vw", height: "100vh" }}>
        <Grid />
      </Box>
    </GridProvider>
  );
}

export default App;
