import axios from "axios";
import {moveRequestHandler} from './handlers'

const HOST = "http://localhost:5000/";


export async function moveRequest(start_pos, end_pos)
{
    const response = await axios.post(HOST + "move_request", {
        start_pos: start_pos,
        end_pos: end_pos
      });

    console.log("hi" + response.data)
    console.log("response: " + JSON.parse(response.data).status)

    return JSON.parse(response.data).status;
    //moveRequestHandler(response, start_pos, end_pos)

}
