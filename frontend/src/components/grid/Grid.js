import React from "react";
import { useGrid } from "../../contexts/GridProvider";
import { Stack } from "@mui/material";
import Node from "./Node";
import MouseInputsProvider from "../../contexts/MouseInputsProvider";

export default function Grid() {
  const [grid, setGrid] = useGrid();

  return (
    <MouseInputsProvider>
      <Stack
        direction="row"
        sx={{
          width: "100%",
          height: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {grid.map((col) => (
          <Stack direction="column">
            {col.map((node) => (
              <Node node={node} />
            ))}
          </Stack>
        ))}
      </Stack>
    </MouseInputsProvider>
  );
}
