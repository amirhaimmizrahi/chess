import "./Node.css";
import { Box, Button } from "@mui/material";
import React, { useState } from "react";
import { useMouseInputs } from "../../contexts/MouseInputsProvider";
import { useGrid } from "../../contexts/GridProvider";
import { moveRequest } from "../../api/move-request/requests";

export default function Node({ node }) {
  const [
    mouseInputs,
    mouseDown,
    mouseUp,
    enableDragging,
    disableDragging,
    setDragData,
  ] = useMouseInputs();

  const [grid, setGrid] = useGrid();

  function handleMouseDown() {
    mouseDown();

    if (mouseInputs.isDragging) return;

    if (node.type) {
      enableDragging();
      setDragData({ position: [...node.position], type: `${node.type}` });
    }
  }

  function handleMouseUp() {
    mouseUp();

    if (mouseInputs.isDragging) {
      const { position, type } = mouseInputs.dragData;

      moveRequest(position, node.position).then((response) => {
        const validMove = response;

        console.log(validMove);
        if (validMove) {
          node.type = type;
          node.ref.current.className = type;

          grid[position[0]][position[1]].type = "";
          grid[position[0]][position[1]].ref.current.className = "";
        }

        console.log("client start pos: " + position);
        console.log("client end pos: " + node.position);
      });
    }

    disableDragging();
  }

  return (
    <Box
      className="node"
      sx={{
        width: "100px",
        height: "100px",
        background:
          (node.position[0] + node.position[1]) % 2 ? "#769656" : "#eeeed2",
      }}
    >
      <Box
        className={node.type}
        ref={node.ref}
        sx={{ width: "100%", height: "100%" }}
      >
        <Button
          sx={{ width: "100%", height: "100%" }}
          onMouseDown={handleMouseDown}
          onMouseUp={handleMouseUp}
        ></Button>
      </Box>
    </Box>
  );
}
