import React, { createContext, useContext, useEffect, useState } from "react";
import { createGrid } from "../utils/grid-utils";

const GridContext = createContext();

export function useGrid() {
  return useContext(GridContext);
}

export default function GridProvider({ children }) {
  const GRID_SIZE = 8;
  const [grid, setGrid] = useState([[]]);

  function initGrid() {
    setGrid(createGrid(GRID_SIZE));
  }

  useEffect(() => {
    initGrid();
  }, []);

  return (
    <GridContext.Provider value={[grid, setGrid]}>
      {children}
    </GridContext.Provider>
  );
}
