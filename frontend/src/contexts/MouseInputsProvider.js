import React, { createContext, useContext, useState } from "react";

const MouseInputsContext = createContext();

const mouseInputs = {
  isMouseDown: false,
  isDragging: false,
  dragData: null,
};

export function useMouseInputs() {
  return useContext(MouseInputsContext);
}

export default function MouseInputsProvider({ children }) {
  function mouseDown() {
    mouseInputs.isMouseDown = true;
  }

  function mouseUp() {
    mouseInputs.isMouseDown = false;
  }

  function enableDragging() {
    mouseInputs.isDragging = true;
  }

  function disableDragging() {
    mouseInputs.isDragging = false;
    mouseInputs.dragData = {};
  }

  function setDragData(data) {
    mouseInputs.dragData = { ...data };
  }

  return (
    <MouseInputsContext.Provider
      value={[
        mouseInputs,
        mouseDown,
        mouseUp,
        enableDragging,
        disableDragging,
        setDragData,
      ]}
    >
      {children}
    </MouseInputsContext.Provider>
  );
}
