import { createRef } from "react";

export function createGrid(size) {
  const tempGrid = [];

  for (let x = 0; x < size; x++) {
    tempGrid.push([]);

    for (let y = 0; y < size; y++) {
      tempGrid[x].push(createNode([x, y]));
    }
  }

  initBlackPieces(tempGrid);
  initWhitePieces(tempGrid);

  return tempGrid;
}

function createNode(position) {
  return { position, type: "", ref: createRef() };
}

function initBlackPieces(grid) {
  grid[0][0].type = "r";
  grid[1][0].type = "kn";
  grid[2][0].type = "b";
  grid[3][0].type = "q";
  grid[4][0].type = "k";
  grid[5][0].type = "b";
  grid[6][0].type = "kn";
  grid[7][0].type = "r";

  for (let i = 0; i < 8; i++) {
    grid[i][1].type = "p";
  }
}

function initWhitePieces(grid) {
  grid[0][7].type = "R";
  grid[1][7].type = "KN";
  grid[2][7].type = "B";
  grid[3][7].type = "Q";
  grid[4][7].type = "K";
  grid[5][7].type = "B";
  grid[6][7].type = "KN";
  grid[7][7].type = "R";

  for (let i = 0; i < 8; i++) {
    grid[i][6].type = "P";
  }
}
